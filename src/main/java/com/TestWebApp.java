package com;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestWebApp extends SpringBootTests {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testLogin() throws Exception {
		mockMvc.perform(get("/testregister")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.name").value("megha")).andExpect(jsonPath("$.dob").value("2018-12-02"))
				.andExpect(jsonPath("$.phone").value("7696762414")).andExpect(jsonPath("$.address").value("bbb"))
				.andExpect(jsonPath("$.aadhar").value(1234)).andExpect(jsonPath("$.pan").value("12345"))
				.andExpect(jsonPath("$.password").value("1234"));

	}

}
