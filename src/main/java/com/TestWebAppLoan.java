package com;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public class TestWebAppLoan extends SpringBootTests {

	@Autowired
	WebApplicationContext webApplicationContext;
	
	private MockMvc mockMvc;
	
	public void setup(){
		
		mockMvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@Test
	public void testUser() throws Exception {
		mockMvc.perform(get("/testloan")).andExpect(status().isOk())
	.andExpect(content().contentType("application/json;charset=UTF-8"))
	.andExpect(jsonPath("$.buildingsize").value(23.4))
	.andExpect(jsonPath("$.pincode").value(60021)).andExpect(jsonPath("$.propAge").value(1)).andExpect(jsonPath("$.propArea").value(10))
	.andExpect(jsonPath("$.loanAmt").value(23.4)).andExpect(jsonPath("$.loanTenure").value(2)) 
	.andExpect(jsonPath("$.oldEmi").value(30.8)).andExpect(jsonPath("$.loanAmt").value(23.4));
	
	
	

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	 
}
